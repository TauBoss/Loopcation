<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class ImportCsvData extends Command
{
    protected $signature = 'import:csv';

    protected $description = 'Import data from CSV files';

    protected $headerMap = [
        'Job Title' => 'job_title',
        'Email Address' => 'email_address',
        'FirstName LastName' => 'first_name_last_name',
        'productname' => 'product_name'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $sources = ['https://backend-developer.view.agentur-loop.com/customers.csv', 'https://backend-developer.view.agentur-loop.com/products.csv'];
        $tables = ['customers', 'products'];
        $counter = 0;
        

        foreach ($sources as $source)
        {
            if (!filter_var($source, FILTER_VALIDATE_URL)) 
            {
                return $this->error('Invalid source. Please provide a valid URL.');
            }

            $this->info('Importing form '.$source);

            $this->importCsvFileFromUrl($source, $tables[$counter++]);
        }

        $this->info('All data imported successfully.');

        return;
    }

    private function importCsvFileFromUrl($url, $table)
    {
        $username = 'loop';         
        $password = 'backend_dev'; 
        $succesfullImports = 0;
        $notSuccessfullImports = 0;

        $response = Http::withBasicAuth($username, $password)->get($url);

        if (!$response->successful()) 
        {
            $this->error('Failed to fetch the CSV file from the URL.');
        } 

        $csvData = $response->body();
        
           // Parse the CSV data
        $lines = explode("\n", $csvData);
        $headers = $this->parseHeaders(str_getcsv(array_shift($lines)));;

        
        If (!Schema::hasTable($table))
        {
            return $this->info('Table ' .$table . ' does not exist');
        }

        foreach ($lines as $line) {
            $data = [];
            $row = str_getcsv($line);

            if (count($row) === count($headers)) 
            {
                $data = array_combine($headers, $row);
            }
            
            if (DB::table($table)->insertOrIgnore($data))
            {
                $succesfullImports++;
                continue;
            }

            $notSuccessfullImports++;
        }

        $this->info("Sucessfull imports " . $succesfullImports);
        $this->info("Could not Import " . $notSuccessfullImports);

        return;
    }

    private function parseHeaders($headers) : Array {

        $newHeaders = [];

        foreach ($headers as $header)
        {
            $newHeaders[] = $this->headerMap[$header] ?? $header;
        }

        return $newHeaders;
    }
}



