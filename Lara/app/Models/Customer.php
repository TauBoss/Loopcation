<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'ID',
        'job_title',
        'email_address',
        'first_name_last_name',
        'registered_since',
        'phone',
    ];

    public function orders() {
        return $this->HasMany(Orders::class);
    }
}
