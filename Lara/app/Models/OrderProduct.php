<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'orders_id',
        'products_id',
        'quantity',
    ];

    public function Orders() {
        return $this->HasMany(Orders::class);
    }

    public function Products() {
        return $this->HasMany(Products::class);
    }
}
