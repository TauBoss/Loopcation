<?php

namespace App\Services;

use App\Models\Orders;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class PaymentService
{
    public function makePayment($customerEamil, $amount, $id)
    {
        
        $requestData = [
            'order_id' => intval($id),
            'customer_email' => $customerEamil,
            'value' => $amount,
        ];

        try 
        {
            $response = Http::post('https://superpay.view.agentur-loop.com/pay', $requestData);

            if (!$response->successful()) 
            {
                return $response->status();
            }

            if ($response->json()['message'] == 'Payment Successful') 
            {
                Orders::where('id', $id)->update(['payed' => true]);
            }

            return $response->json();

        } 
        catch (\Exception $e) 
        {
            return response()->json(['message' => 'Ups payment did not work' .$e], 418);
        }

    }
}