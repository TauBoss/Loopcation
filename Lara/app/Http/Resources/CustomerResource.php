<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'jobTitle' => $this->job_title,
            'emailAddress' => $this->email_address,
            'firstNameLastName' => $this->first_name_last_name,
            'registeredSince' => $this->registered_since,
            'phone' => $this->phone,
        ];
    }
}
