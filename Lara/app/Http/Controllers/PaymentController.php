<?php

namespace App\Http\Controllers;

use App\Http\Requests\PayOrderRequest;
use App\Models\Orders;
use App\Services\PaymentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    protected $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    public function pay(PayOrderRequest $request, $id)
    {
        $order = Orders::where('id', $id)->get()->first();

        if ($order->payed == true)
        {
            return response()->json(['message' => 'Order already payed'], 418);
        }
        
        return $this->paymentService->makePayment($request->customer_email, $request->value, $id);
    }

}
