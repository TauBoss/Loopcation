<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddProductRequest;
use App\Http\Requests\StoreOrdersRequest;
use App\Http\Requests\UpdateOrdersRequest;
use App\Http\Resources\OrdersCollection;
use App\Http\Resources\OrdersResource;
use App\Models\OrderProduct;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new OrdersCollection(Orders::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOrdersRequest $request)
    {
        return new OrdersResource(Orders::create($request->all()));
    }

    /**
     * Display the specified resource.
     */
    public function show(Orders $order)
    {
        return new OrdersResource($order);
    }

    /**
     * Adds a product to an order.
     */
    public function addItem(AddProductRequest $request, $id)
    {
        $order = Orders::where('id', $id)->get()->first();

        if ($order->payed == false)
        {
            return  OrderProduct::create([
                        'orders_id' => $id,   
                        'products_id' => $request->product_id,      
                        'quantity' => 1,     
                    ]);
        }
        
        return response()->json(['message' => 'Order is already payed'], 418);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOrdersRequest $request, Orders $order)
    {
        $order->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Orders $order)
    {
        $order->delete();
    }
}
