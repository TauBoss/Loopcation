<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

        /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'jobTitle' => ['Sometimes'],
            'emailAddress' => ['required', 'email'],
            'firstNameLastName' => ['required'],
            'registeredSince' => ['required'],
            'phone' => ['required'],
        ];
    }

    protected function prepareForValidation()
    {
        if ($this->jobTitle) {
            $this->merge([
                'job_title' => $this->jobTitle
            ]);
        }

        $this->merge([
            'email_address' => $this->emailAddress,
            'first_name_last_name' => $this->firstNameLastName,
            'registered_since' => $this->registeredSince,
        ]);

    }
}

