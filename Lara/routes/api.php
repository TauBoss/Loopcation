<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([], function() {
    Route::apiResource('customers', CustomerController::class);
    Route::apiResource('products', ProductsController::class);
    Route::apiResource('orders', OrdersController::class);

    Route::post('orders/{id}/add', 'App\Http\Controllers\OrdersController@addItem');
    Route::post('orders/{id}/pay', 'App\Http\Controllers\PaymentController@pay');
});