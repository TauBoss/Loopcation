# Loopcation

I set up a docker conatiner for the application using Laravel Sail.

Make sure you have docker installed and running.

After cloning the repository, go into the Lara folder and execute following commands.

```c
//downloads and installs the dependencies
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs

//starts and builds the docker container
./vendor/bin/sail up --build

//migrates the tables into the database
./vendor/bin/sail php artisan migrate
```


Then use `./vendor/bin/sail php artisan import:csv` to import the data form the CSV files.